<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new \App\User;
		$user->name = "Administrator";
		$user->email = "adminpa1@bsi.ac.id";
		$user->password = \Hash::make( "admin");
		$user->remember_token = "token";
		$user->save();
		$this->command->info( "User Admin berhasil diinsert");
		}
}
